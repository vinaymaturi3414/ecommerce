from rest_framework import routers
from .api import RestViewSet,ProductViewSet


router=routers.DefaultRouter()
router.register('orders',RestViewSet,'shoppingapp')
router.register('Products',ProductViewSet,'shoppingapp')

urlpatterns =router.urls
