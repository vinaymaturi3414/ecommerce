from shoppingapp.models import Orders,Products
from rest_framework import viewsets,permissions
from shoppingapp.serializers import OrdersSerializer,ProductSerializer

#Rest Viewset

class RestViewSet(viewsets.ModelViewSet):
    queryset=Orders.objects.all()
    permissions_classes=[
        permissions.AllowAny
    ]
    serializer_class= OrdersSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    permissions_classes=[
        permissions.AllowAny
    ]
    serializer_class = ProductSerializer
