from django.contrib import admin
from shoppingapp.models import Orders,Products,Orders_items

# Register your models here.
admin.site.register(Products)
admin.site.register(Orders)
admin.site.register(Orders_items)
